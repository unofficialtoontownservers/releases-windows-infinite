# releases
Releases.

## Issues
If the game gives a DLL error such as with VRCUNTIME140_1.dll, install the Visual Studio Redistributables down below.

https://aka.ms/vs/16/release/vc_redist.x64.exe

https://aka.ms/vs/16/release/vc_redist.x86.exe

### Disclaimer
I own none of the base client/server code, all credit goes to the Toontown Infinite team.
